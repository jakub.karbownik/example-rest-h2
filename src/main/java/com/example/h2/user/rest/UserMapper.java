package com.example.h2.user.rest;

import com.example.h2.user.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {

    User toUser(UserEntity entity);

    List<User> toUserList(List<UserEntity> entityList);

    @Mapping(target = "id", ignore = true)
    UserEntity createUserEntity(User user);

    @Mapping(target = "id", ignore = true)
    void updateUserEntity(@MappingTarget UserEntity entity, User user);
}
