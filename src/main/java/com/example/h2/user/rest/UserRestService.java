package com.example.h2.user.rest;

import com.example.h2.user.UserEntity;
import com.example.h2.user.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserRestService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;

    public List<User> getUsers() {
        Sort sort = Sort.by("id");
        List<UserEntity> entities = userRepository.findAll(sort);
        return userMapper.toUserList(entities);
    }

    public User getUserById(int id) {
        UserEntity entity = userRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException(id));
        return userMapper.toUser(entity);
    }

    public User createUser(User user) {
        log.info("Create user: {}", user);
        UserEntity entity = userMapper.createUserEntity(user);
        userRepository.save(entity);
        return userMapper.toUser(entity);
    }

    public User updateUser(int id, User user) {
        log.info("Update user: {}, {}", id, user);
        UserEntity entity = userRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException(id));
        userMapper.updateUserEntity(entity, user);
        userRepository.save(entity);
        return userMapper.toUser(entity);
    }

    public void deleteUser(int id) {
        log.info("Delete user: {}", id);
        UserEntity entity = userRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException(id));
        userRepository.delete(entity);
    }
}
