package com.example.h2.user.rest;

public class UserNotFoundException extends RuntimeException {

    public UserNotFoundException(String message) {
        super(message);
    }

    public UserNotFoundException(int id) {
        super("User not found: " + id);
    }
}
