package com.example.h2;

import com.example.h2.user.rest.User;
import com.example.h2.user.rest.UserRestService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@RequiredArgsConstructor
@Slf4j
public class DataProvider {

    private final UserRestService userRestService;

    @PostConstruct
    protected void initData() {
        log.info("Adding initially 2 users");
        userRestService.createUser(new User(0, "user1", "1111", "user1@example.com"));
        userRestService.createUser(new User(0, "user2", "2222", "user2@example.com"));
    }
}
